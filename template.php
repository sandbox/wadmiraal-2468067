<?php

/**
 * @file
 * Theme template logic.
 */

/**
 * Implements template_preprocess_page().
 */
function tabard_preprocess_page(&$vars) {
  $vars['herald_menu'] = array();

  foreach (array(
    'projects' => array(
      'title' => t("My projects"),
      'class' => 'icon-projects',
    ),
    'node/add/herald-project' => array(
      'title' => t("Add a new project"),
      'class' => 'icon-add-project',
    ),
    'search/node' => array(
      'title' => t("Search"),
      'class' => 'icon-search',
    ),
    'https://www.drupal.org/project/issues/herald' => array(
      'title' => t("Bug report"),
      'class' => 'icon-bug',
    ),
    drupal_get_normal_path('about') => array(
      'title' => t("About Herald"),
      'class' => 'icon-about',
    ),
    'contact' => array(
      'title' => t("Help"),
      'class' => 'icon-help',
    ),
    'user' => array(
      'title' => t("My account"),
      'query' => array(
        'destination' => current_path(),
      ),
      'class' => 'icon-user',
    ),
  ) as $path => $override) {

    if (preg_match('/^https?:\/\//', $path)) {
      $vars['herald_menu'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => array(
          'href' => $path,
          'title' => '<span>' . (!empty($override['title']) ? $override['title'] : $path) . '</span>',
          'localized_options' => array(
            'html' => TRUE,
            'attributes' => array(
              'title' => !empty($override['title']) ? $override['title'] : $path,
              'class' => array(
                !empty($override['class']) ? $override['class'] : NULL,
              ),
            ),
          ),
        ),
      );
    }
    else {
      $link = menu_get_item($path);

      if (!empty($link) && $link['access']) {
        if (!empty($override['title'])) {
          $link['title'] = $link['localized_options']['attributes']['title'] = $override['title'];
        }
        if (!empty($override['class'])) {
          $link['localized_options']['attributes']['class'][] = $override['class'];
        }
        if (!empty($override['query'])) {
          if (!isset($link['localized_options']['query'])) {
            $link['localized_options']['query'] = array();
          }
          $link['localized_options']['query'] += $override['query'];
        }
        $link['localized_options']['html'] = TRUE;
        $link['title'] = '<span>' . check_plain($link['title']) . '</span>';
        $vars['herald_menu'][] = array(
          '#theme' => 'menu_local_action',
          '#link' => $link,
        );
      }
    }
  }
}

/**
 * Implements template_preprocess_node().
 */
function tabard_preprocess_node(&$vars) {
  $node = $vars['node'];

  switch($node->type) {
    case 'herald_project':
      if (function_exists('herald_load_latest_project_build')) {
        $build = herald_load_latest_project_build($node);

        if ($build) {
          $vars['herald_latest_build'] = node_view($build, 'full');
        }
      }
      break;

    case 'herald_build':
      if (function_exists('herald_load_build_tasks')) {
        $tasks = herald_load_build_tasks($node);

        if ($tasks) {
          $vars['herald_tasks'] = array();

          foreach ($tasks as $task) {
            $vars['herald_tasks'][] = node_view($task, 'full');
          }
        }
      }
      break;
  }
}

/**
 * Implements template_preprocess_menu_local_task().
 */
function tabard_preprocess_menu_local_task(&$vars) {
  if (!isset($vars['element']['#link']['localized_options']['attributes'])) {
    $vars['element']['#link']['localized_options']['attributes'] = array();
  }
  if (!isset($vars['element']['#link']['localized_options']['attributes']['class'])) {
    $vars['element']['#link']['localized_options']['attributes']['class'] = array();
  }
  $vars['element']['#link']['localized_options']['attributes']['class'][] = str_replace(array('/', '%'), array('-', ''), $vars['element']['#link']['path']);
}
