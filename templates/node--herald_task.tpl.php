<?php

/**
 * @file
 * Our theme implementation to display a task node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix node-herald-task--<?php print $content['herald_task_status']['#items'][0]['value']; ?>"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="node-herald-task__content content"<?php print $content_attributes; ?>>
    <?php print render($content['herald_task_type']); ?>

    <?php if ($content['herald_task_status']['#items'][0]['value'] != 'finished'): ?>
      <div class="node-herald-task__content__pending-message">
        <?php print $content['herald_task_status']['#items'][0]['value'] == 'queued' ? t("Task is queued") : t("Task is running") . '&hellip;'; ?>
      </div>
    <?php else:
      hide($content['comments']);
      hide($content['links']);
      hide($content['herald_build_ref']);
      print render($content);
    endif; ?>
  </div>
</div>
