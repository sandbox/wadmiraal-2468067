<?php

/**
 * @file
 * Our theme implementation to display a project node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="node-herald-project__content content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
      hide($content['herald_build_types']);
      hide($content['herald_task_types']);
      hide($content['herald_project_core_version']);
    ?>

    <div class="node-herald-project__content__column">
      <?php print render($content); ?>
    </div>

    <?php if ($page): ?>
      <div class="node-herald-project__content__column">
        <dl>
          <dt><?php print t("Core"); ?>:</dt>
          <dd><?php print $content['herald_project_core_version'][0]['#markup']; ?></dd>

          <dt><?php print t("Build mode(s)"); ?>:</dt>
          <dd>
            <?php print implode(', ', array_filter(array_map(function($item, $key) {
              if (is_numeric($key)) {
                return $item['#markup'];
              }
            }, $content['herald_build_types'], array_keys($content['herald_build_types'])))); ?>
          </dd>

          <dt><?php print t("Task type(s)"); ?>:</dt>
          <dd>
            <?php print implode(', ', array_filter(array_map(function($item, $key) {
              if (is_numeric($key)) {
                return $item['#markup'];
              }
            }, $content['herald_task_types'], array_keys($content['herald_task_types'])))); ?>
          </dd>
        </dl>
      </div>

      <?php if (!empty($herald_latest_build)): ?>
        <div class="node-herald-project__content__build">
          <h3><?php print t("Latest build"); ?></h3>

          <?php print render($herald_latest_build); ?>
        </div>
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>
