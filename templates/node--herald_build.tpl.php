<?php

/**
 * @file
 * Our theme implementation to display a build node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><?php print t("Build"); ?>: <span><?php print $title; ?></span></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="node-herald-buil__content content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
      hide($content['herald_build_type']);
      hide($content['herald_project_ref']);
      print render($content);
    ?>

    <?php if (!empty($herald_tasks)): ?>
      <div class="node-herald-build__content__tasks">
        <?php print render($herald_tasks); ?>
      </div>
    <?php endif; ?>
  </div>
</div>
