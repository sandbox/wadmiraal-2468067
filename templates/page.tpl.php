<?php

/**
 * @file
 * Template for displaying a Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div class="site">
  <div class="site__menu">
    <nav class="site__menu__herald-menu">
      <ul class="menu">
        <?php print render($herald_menu); ?>
      </ul>
    </nav>

    <?php print render($page['sidebar_first']); ?>
  </div>

  <div class="site__content">
    <div class="site__content__tabs-wrapper">
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="site__content__tabs-wrapper__title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($tabs): ?>
        <div class="site__content__tabs-wrapper__tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
    </div>

    <?php print $messages; ?>

    <div class="site__content__help">
      <?php print render($page['help']); ?>
    </div>

    <?php if ($action_links): ?>
      <ul class="site__content__action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>

    <div class="site__content__content">
      <?php print render($page['content']); ?>
    </div>

    <div class="site__content__footer">
      <?php print render($page['footer']); ?>
    </div>
  </div>
</div>
